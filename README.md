This git repository serves as a proof of concept for showing the ability to set arbitrary git author dates and git committer dates.

Consider the following block in the bitcoin blockchain:
```
Height: 705,507
Hash: 0000000000000000000967316c8ea264b30e5f87328793bcf4d4c28c270e792b
Nonce: 2,898,759,154
```

It was created on October 18, 2021. However, this README.md file has been committed on a prior date.
